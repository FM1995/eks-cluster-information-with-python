# EKS cluster information with Python

#### Project Outline

So far have utilized python with EC2 instances, now lets utilise it with getting EKS information which we have deployed previously using terraform. Using python we can get relevant information like EKS cluster status, which Kubernetes version and the cluster endpoint

#### Lets get started

Lets begin creating our EKS cluster using terraform, will use one of the previously created projects.

[eks-cluster.tf Terraform Script](https://gitlab.com/FM1995/terraform-eks-lab/-/blob/main/eks-cluster.tf?ref_type=heads)


![Image 1](https://gitlab.com/FM1995/eks-cluster-information-with-python/-/raw/main/Images/Image1.png)

Can then apply it using the below

```
terraform apply –auto-approve
```

And can see the resources got created

![Image 2](https://gitlab.com/FM1995/eks-cluster-information-with-python/-/raw/main/Images/Image2.png)

![Image 3](https://gitlab.com/FM1995/eks-cluster-information-with-python/-/raw/main/Images/Image3.png)

Now using the documentation


https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/eks.html

![Image 4](https://gitlab.com/FM1995/eks-cluster-information-with-python/-/raw/main/Images/Image4.png)

What we want to do is list the cluster and complete a health check

Again using the below documentation

https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/eks/client/list_clusters.html

Can then print the clusters, importing the boto3 client, set the region to eu-west-2 london and list the cluster and then print it out

And can see it is a success

![Image 5](https://gitlab.com/FM1995/eks-cluster-information-with-python/-/raw/main/Images/Image5.png)

Now we want to describe the cluster

Can also use the below documentation to describe cluster

https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/eks/client/describe_cluster.html

Can then configure and include the loop in the code

![Image 6](https://gitlab.com/FM1995/eks-cluster-information-with-python/-/raw/main/Images/Image6.png)

Can further re-define and get the status

![Image 7](https://gitlab.com/FM1995/eks-cluster-information-with-python/-/raw/main/Images/Image7.png)

![Image 8](https://gitlab.com/FM1995/eks-cluster-information-with-python/-/raw/main/Images/Image8.png)

Now ready to execute

And can see it is a success with ‘ACTIVE’ status

![Image 9](https://gitlab.com/FM1995/eks-cluster-information-with-python/-/raw/main/Images/Image9.png)

Can then further re-define and get the name 

![Image 10](https://gitlab.com/FM1995/eks-cluster-information-with-python/-/raw/main/Images/Image10.png)

Can also run it using to get the endpoint as defined in the documentation, can see the below

![Image 11](https://gitlab.com/FM1995/eks-cluster-information-with-python/-/raw/main/Images/Image11.png)

Can then further improve and then include the version

![Image 12](https://gitlab.com/FM1995/eks-cluster-information-with-python/-/raw/main/Images/Image12.png)

And below are the results


![Image 13](https://gitlab.com/FM1995/eks-cluster-information-with-python/-/raw/main/Images/Image13.png)




